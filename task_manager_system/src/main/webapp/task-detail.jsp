<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true" 
    import="java.util.List,model.entity.TaskBean,model.entity.CommentBean,model.entity.UserBean"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Task Detail</title>
		<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
		<link rel ="stylesheet" href ="task-detail.css">
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<link href="sidebar.css" rel="stylesheet"/>
		<!-- Font Awesome -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
		<!-- MDB -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.js"></script>
	</head>
	<body>
	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer
            mdl-layout--fixed-header">
<div class="mdl-layout__drawer">
<span class="title">Task Management System</span>
<nav class="mdl-navigation">
<a class="task" href="task-list-servlet">Task List</a>
<a class="task" href="task-register-transition-servlet">New Task</a>
<a class="logout" id="show-dialog-logout">Logout</a>
</nav>
</div>
  <main class="mdl-layout__content">
    <div class="page-content">
	
		<div class="card">
			<div class = "card-title">
		  
			</div>
			<%
				request.setCharacterEncoding("UTF-8");
				TaskBean task = (TaskBean)request.getAttribute("task");
			%>
		
			<div class="card-body">
				<div class="menu">
						
						<button id="demo-menu-lower-right"
							     class="mdl-button mdl-js-button mdl-button--icon">
							<span class="material-symbols-outlined">
							delete
							</span>
						</button>
						
						<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
						    for="demo-menu-lower-right">
						  	<li class="mdl-menu__item" id="show-dialog">Delete the Task</li>
						  	</ul>
							<dialog class="mdl-dialog">
								<h4 class="mdl-dialog__title">Allow to delete the Task?</h4>
								<div class="mdl-dialog__content">
									<p>Deleted task cannot be restored.</p>
								</div>
								<div class="mdl-dialog__actions">
									<form action="task-delete-servlet" method="POST">
										<button type="submit" class="mdl-button" name="taskId" value=<%=task.getTaskId() %>>OK</button>
									</form>
										<button type="button" class="mdl-button close">cancel</button>
									
								</div>
							</dialog>
							 <script>
							    var dialog = document.querySelector('dialog');
							    var showDialogButton = document.querySelector('#show-dialog');
							    if (! dialog.showModal) {
							      dialogPolyfill.registerDialog(dialog);
							    }
							    showDialogButton.addEventListener('click', function() {
							      dialog.showModal();
							    });
							    dialog.querySelector('.close').addEventListener('click', function() {
							      dialog.close();
							    });
							  </script>
						
				</div>
				<dialog class="mdl-dialog" id="dialog-logout">
								<h4 class="mdl-dialog__title">Are you sure you want to log out?</h4>
								<div class="mdl-dialog__actions">
									<form action="logout-servlet" method="POST">
										<button type="submit" class="mdl-button">OK</button>
									</form>
										<button type="button" class="mdl-button close">cancel</button>
									
								</div>
							</dialog>
							 <script>
							    var dialogLogout = document.querySelector('#dialog-logout');
							    var showDialogLogoutButton = document.querySelector('#show-dialog-logout');
							    if (! dialogLogout.showModal) {
							      dialogPolyfill.registerDialog(dialogLogout);
							    }
							    showDialogLogoutButton.addEventListener('click', function() {
							      dialogLogout.showModal();
							    });
							    dialogLogout.querySelector('.close').addEventListener('click', function() {
							      dialogLogout.close();
							    });
							  </script>
				<div class="clip-box-a">
					<div>
						
				    	<%--タスク名表示 --%>
				    	<span style="font-size: 40px;"><%= task.getTaskName() %></span>
				    	
						<%--ステータス表示 --%>
						<span style="margin-left: 8%; margin-right: 5%;"><%= task.getStatus().getName() %></span>
						
						<%--期限表示 --%>
						DeadLine　▶　<%= task.getLimitDate() %>
						
					</div>
					<div>
				    	<%--カテゴリ表示 --%>
				    	<span style="color: #a0a0a0; "><%= task.getCategory().getName() %></span>
					</div>
					<br>
					<div>
				    	<%--担当者表示 --%>
				    	<span style="font-size: 30px"><%= task.getUser().getUserName() %></span>
					</div>
					
					<div>
						<%--ステータス表示 --%>
						RegiTime　▶　<%= task.getRegistrationTime() %>
					</div>
					<div>
						<%--ステータス表示 --%>
						UpdateTime　▶　<%= task.getUpdateTime() %>
					</div>
					
					<div class="kakomi-box8">
			    		Memo ▼<br>
			    		<%
			    		String memo = task.getMemo() == null ? "" : task.getMemo();
			    		%>
			    		<%=memo %>
					</div>
				</div>
				<div class="btn-position">
						<%--タスク一覧に戻る --%>
						<form action="task-list-servlet" method="GET">
							
								<input type="hidden" value=<%=task.getTaskId() %> name="taskId" class="btn-btn-success">
								<button type = "submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
								    <div class = "btn-font">Back List</div>
								</button>
	            			
						</form>
						<%--タスク編集 --%>
						<form action="task-edit-transition-servlet" method="GET" class="edit-btn">
							
								<input type="hidden" value=<%=task.getTaskId() %> name="taskId" class="btn-btn-success">
								<button type = "submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
								    <div class = "btn-font">Edit</div>
								</button>
	            			
						</form>
				</div>
				<div class="comments-area" style="margin-top: 25px">
					<div>
						
						<h3>Comments</h3>
						<% 
							List<CommentBean> comments = (List<CommentBean>) request.getAttribute("comments");
						%>
						<%
							for (CommentBean comment : comments) {
								String iconPath = comment.getUser().getIconPath();
								String userName = comment.getUser().getUserName();
								String com = comment.getComment();
						%>
								<ul class="demo-list-three mdl-list">
								  <li class="mdl-list__item mdl-list__item--three-line">
								    <span class="mdl-list__item-primary-content">
								      <i class="mdl-list__item-avatar"><img src = <%=iconPath %> style="width: 45px; border-radius: 50%;"></i>
								      <span style="font-size: 8px"><%=userName %></span>
								      <span class="mdl-list__item-text-body">
								        
								        <span style="font-family: cursive; "><%=com %></span>
								      </span>
								     </span>
								    
								  </li>
								</ul>
						<%
							}
						%>
					</div>
						
					<div class="comments">	
						<form action="comment-servlet" method="POST">
							<div class="send-area">
								<div class="comment-box" style="margin-left: 10%;">
									<textarea name="comment" cols="50" rows="2"></textarea>
								</div>
								<div class="btn-btn-success" style="margin-left: 10%; margin-top: 13px;">
									<input type="hidden" value=<%=task.getTaskId() %> name="task-id">
									
									<button type = "submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
									    <div class = "btn-font">Send</div>
									</button>
		            			</div>
		            		</div>
						</form>
						
						<%
							Boolean isFailure = (Boolean)request.getAttribute("isFailure");
							if ( isFailure != null && isFailure) {
						%>
						<%= request.getAttribute("errorMessage") %>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
		  </div>
  </main>
</div>
	</body>
</html>
				