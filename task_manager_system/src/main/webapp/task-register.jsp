<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="model.entity.CategoryBean, model.entity.UserBean, model.entity.StatusBean, java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New Task</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<link href="sidebar.css" rel="stylesheet"/>

<!-- Font Awesome -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
<!-- MDB -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.css" rel="stylesheet"/>
<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.js"></script>
<!-- material -->
<link rel = "stylesheet" href = "task-register.css">
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer
            mdl-layout--fixed-header">
<div class="mdl-layout__drawer">
<span class="title">Task Management System</span>
<nav class="mdl-navigation">
<a class="task" href="task-list-servlet">Task List</a>
<a class="task" href="task-register-transition-servlet">New Task</a>
<a class="logout" id="show-dialog">Logout</a>
</nav>
</div>
<dialog class="mdl-dialog">
								<h4 class="mdl-dialog__title">Are you sure you want to log out?</h4>
								<div class="mdl-dialog__actions">
									<form action="logout-servlet" method="POST">
										<button type="submit" class="mdl-button">OK</button>
									</form>
										<button type="button" class="mdl-button close">cancel</button>
									
								</div>
							</dialog>
							 <script>
							    var dialog = document.querySelector('dialog');
							    var showDialogButton = document.querySelector('#show-dialog');
							    if (! dialog.showModal) {
							      dialogPolyfill.registerDialog(dialog);
							    }
							    showDialogButton.addEventListener('click', function() {
							      dialog.showModal();
							    });
							    dialog.querySelector('.close').addEventListener('click', function() {
							      dialog.close();
							    });
							  </script>
  <main class="mdl-layout__content">
    <div class="page-content">

<div class="card">
        <div class = "card-title">
		  <p>New Task</p>
		</div>
		<div class="blockform">
	<div class="card-body">
	<br>
<% 
	List<UserBean> userList = (List<UserBean>)request.getAttribute("userList");
	List<CategoryBean> categoryList = (List<CategoryBean>)request.getAttribute("categoryList");
	List<StatusBean> statusList = (List<StatusBean>)request.getAttribute("statusList");
%>
<%
	String errorChecker = (String)request.getAttribute("errorChecker");
	if("error".equals(errorChecker)){ 
		String errorMessage = (String)request.getAttribute("errorMessage");
%>
		<p style="color:#ff0000;"><%=errorMessage %></p>
<%}%>
<div class="register-form">
<form action="task-register-servlet" method="post">

<div class="mdl-textfield mdl-js-textfield">
Task Name:
<input class="mdl-textfield__input" type="text" name="taskName" required>
<br>
Category Name:<br>
<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
<input id="option-1" class="mdl-radio__button, js-select" type="radio" name="new_or_exist" value="exist" onclick="formSwitch()" checked>Select
<input id="option-1" class="mdl-radio__button, js-select" type="radio" name="new_or_exist" value="new" onclick="formSwitch()">Register
</label>
  <span id = "selectExisting">
    <select id = "changeSelect" name = "existCategoryId">
    
    <% for( CategoryBean category : categoryList) { %>
    
      <option value="<%=category.getId() %>"><%=category.getName() %></option>
     <% } %>
     </select>
  </span>
  <span id = "selectNew">
	<input id = "newCategory" class="mdl-textfield__input" type = "text" name = "newCategoryName">
  </span>
  <br>
Limit Date:<br>
<input type="date" name="limitDate" required><br>
<br>
Person：<br>
<select name="userId">
<% for(UserBean user : userList) { %>
      <option value="<%=user.getUserId() %>"><%=user.getUserName() %></option>
<%} %>    
</select><br>
<br>
Status:<br>
<select name="statusCode">
<option value="0">Not started</option>
<option value="50">Underway</option>
</select><br>
<br>
Memo:
<textarea class="mdl-textfield__input" name="memo"></textarea>
<br>
<br>
<br>
<br>
<div style="display: flex;justify-content: center;">
<button type = "submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"><div style="font-family: cursive;">Submit</div></button>
</div>
</form>
</div>
</div>
</div>
</div>

<script>
    var selectBox = document.getElementById('selectExisting');
    var text = document.getElementById('selectNew');
    var newCategory = document.getElementById('newCategory');
    
    selectBox.style.display = "none";
    text.style.display = "none";
    newCategory.required=false;
    
    function formSwitch() {
        check = document.getElementsByClassName('js-select')
        if (check[0].checked) {
            selectBox.style.display = "block";
	    text.style.display = "none";
	    newCategory.required=false;
        } else if (check[1].checked) {
	    selectBox.style.display = "none";
	    text.style.display = "block";
	    newCategory.required=true;

        } else {
        }
    }
    window.addEventListener('load', formSwitch());
    
  </script>
  </div>
  </main>
</div>
</body>
</html>