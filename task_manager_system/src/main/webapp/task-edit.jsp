<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" 
    import="java.util.List, model.entity.TaskBean,
    model.entity.CategoryBean, model.entity.UserBean, 
    model.entity.StatusBean, model.entity.CommentBean"%>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>edit</title>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<link href="sidebar.css" rel="stylesheet"/>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
		<!-- MDB -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.css" rel="stylesheet"/>
		<!-- MDB -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.js"></script>
		<!-- material -->
		<link rel = "stylesheet" href = "task-edit.css">
	</head>
	
	<body>
		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer
            mdl-layout--fixed-header">
<div class="mdl-layout__drawer">
<span class="title">Task Management System</span>
<nav class="mdl-navigation">
<a class="task" href="task-list-servlet">Task List</a>
<a class="task" href="task-register-transition-servlet">New Task</a>
<a class="logout" id="show-dialog">Logout</a>
</nav>
</div>
<dialog class="mdl-dialog">
								<h4 class="mdl-dialog__title">Are you sure you want to log out?</h4>
								<div class="mdl-dialog__actions">
									<form action="logout-servlet" method="POST">
										<button type="submit" class="mdl-button">OK</button>
									</form>
										<button type="button" class="mdl-button close">cancel</button>
									
								</div>
							</dialog>
							 <script>
							    var dialog = document.querySelector('dialog');
							    var showDialogButton = document.querySelector('#show-dialog');
							    if (! dialog.showModal) {
							      dialogPolyfill.registerDialog(dialog);
							    }
							    showDialogButton.addEventListener('click', function() {
							      dialog.showModal();
							    });
							    dialog.querySelector('.close').addEventListener('click', function() {
							      dialog.close();
							    });
							  </script>
  <main class="mdl-layout__content">
    <div class="page-content">
	<div class="card">
		<div class="card-title">
			<p>Task Edit</p>
		</div>
		<div class="blockform">
			<div class="card-body">
				<br>
				<% 
					String errorChecker = (String) request.getAttribute("errorChecker");
					if ( "error".equals(errorChecker) ) { 
					String errorMessage = (String) request.getAttribute("errorMessage");
				%>
				<p style="color:red"><%=errorMessage %></p>
				<%} %>
				
				<div class="register-form">
					<form action="task-edit-servlet" method="POST">
						<%
							TaskBean task = (TaskBean) request.getAttribute("task");
						%>
						
						<div class="mdl-textfield mdl-js-textfield">
							Task Name：<input type="text" name="taskName" value="<%=task.getTaskName() %>" required>
							<input type="hidden" name="taskId" value="<%=task.getTaskId() %>">
							<br>
							
							Category Name：<br>
							<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
								<input id="option-1" class="mdl-radio__button, js-select" type="radio" name="new_or_exist" value="exist" onclick="formSwitch()" checked>Select
								<input id="option-1" class="mdl-radio__button, js-select" type="radio" name="new_or_exist" value="new" onclick="formSwitch()">Register
							</label>
							
							<!-- <input class="js-select" type="radio" name="new_or_exist" value="exist" checked onclick="formSwitch()">select
							<input class="js-select" type="radio" name="new_or_exist" value="new" onclick="formSwitch()">register
							-->
							<span id = "selectExisting">
								<%
									List<CategoryBean> categoryList =  
									(List<CategoryBean>) request.getAttribute("categoryList");
								%>
								<select id = "changeSelect" name = "existCategoryId">
								
								<%
									for (CategoryBean category : categoryList) {
								%>
									
								<option 
								<%
									if ( category.equals(task.getCategory()) ) {
								%>
										selected
									<%} %>
									value="<%=category.getId() %>"> <%=category.getName() %> </option>
								<%} %>
								</select> 
							</span>
							
							<span id = "selectNew">
								<input class = "mdl-textfield_input" type="text" name = "newCategoryName">
							</span> 
							<br>
							
							<%
								List<UserBean> userList =  (List<UserBean>) request.getAttribute("userList");
							%>
							
							Limit Date：<br>
							<input type="date" name="limitDate" value="<%=task.getLimitDate() %>"><br>
							<br>
							
							Person：<br>
							<select name="user_id">
								<%
									for ( UserBean user : userList ) {
								%>
								
								<option 
								<%
									if ( user.equals(task.getUser()) ) {
								%>
									selected
								<%} %>
								value="<%=user.getUserId() %>"><%=user.getUserName() %>
								 </option>
								<%} %>
								
								<%
									List<StatusBean> statusList =  (List<StatusBean>) request.getAttribute("statusList");
								%>
							</select><br>
							<br>
							
							Status：<br>
							<select name="status_id">
								<%
									for ( StatusBean status : statusList ) {
								%>
									<option 
									<%
										if ( status.equals(task.getStatus()) ) {
									%>
										selected
									<%} %>
									value="<%=status.getCode() %>"><%=status.getName() %>
									</option>
								<%} %>
							</select><br>
							<br>
							
							<%
								List <CommentBean> commentList = (List<CommentBean>) request.getAttribute("commentList");
							%>
							<%
								String memo = (String) request.getAttribute("memo");
							%>
							<!--  <br>-->
							Memo：
							<br>
							<textarea class="mdl-textfield_input" name="editMemo"><%=memo%></textarea>
							
							<br>
							<br>
							<br>
							<br>
							<div style="display:flex;justify-content: center;">
								<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
									<div style="font-family: cursive;">Submit</div>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<script>
		var selectBox = document.getElementById('selectExisting');
		var text = document.getElementById('selectNew');
	 
	    selectBox.style.display = "none";
	    text.style.display = "none";
	
	    function formSwitch() {
	        check = document.getElementsByClassName('js-select')
	        if (check[0].checked) {
	            selectBox.style.display = "block";
		    text.style.display = "none";
	
	        } else if (check[1].checked) {
		    selectBox.style.display = "none";
		    text.style.display = "block";
	
	        } else {
	        }
	    }
	    window.addEventListener('load', formSwitch());
	
	</script>
	  </div>
  </main>
</div>
	</body>
</html>