<%@page import="model.entity.TaskBean"%>
<%@page import="java.util.List"%>
<%@page import="model.entity.CategoryBean" %>
<%@page import="model.entity.UserBean" %>
<%@page import="model.entity.StatusBean" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="jp">
<head>
  <meta charset="UTF-8">
  <title>Task List</title>
  <link rel="stylesheet" href="task-list.css">
    <link rel="stylesheet" href="header-sidebar.css">
      <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Material Design icon font -->
</head>
<body>

<% 
List<CategoryBean> categories = (List)request.getAttribute("categories");
List<TaskBean> tasks = (List<TaskBean>)request.getAttribute("tasks");
List<UserBean> users = (List<UserBean>)request.getAttribute("users");
List<StatusBean> statuses = (List<StatusBean>)request.getAttribute("statuses");
CategoryBean categorybean = (CategoryBean)session.getAttribute("category");
%>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
<header class="mdl-layout__header">
<div class="mdl-layout__header-row">
<h1><%=categorybean.getName()%></h1>
<div id="app" class="app">
  <button class="button"><img src="images/change.png" width="20" height="20" class="change"></button>
  <div class="element1">
  <select id="change-project" name="category">
                      <option value="">--change project--</option>
        	    <%
				if (!(categorybean.getName().equals("ALL Projects"))) {
				%>
				      <option value="task-filter-servlet?category-id=0&category-name=ALL Projects">ALL Procjects</option>
				<%
				}
        	    for (CategoryBean category : categories) {
    				if (category.getId() == categorybean.getId()) {
    					continue;
    				}
    			%>
    			      <option value="task-filter-servlet?category-id=<%=category.getId() %>&category-name=<%=category.getName() %>"><%=category.getName() %></option>
    			<%	
        	    }
				%>
</select>
  </div>
</div>

<div class="mdl-layout-spacer"></div>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right search">
<form action="task-search-servlet" name="search1" method="get">
	<dl class="search1">
		<dt><input type="text" name="task-name" value="<%
		if (!(request.getAttribute("search") == null || request.getAttribute("search").equals(""))) {
		%><%=(String)request.getAttribute("search")%><%}%>" placeholder="search" /></dt>
		<dd><button><span><img src="images/search.png" width="20" height="20"></span></button></dd>
	</dl>
</form>
</div>
</div>
</header>
<div class="mdl-layout__drawer">
<span class="title">Task Management System</span>
<nav class="mdl-navigation">
<a class="task" href="task-list-servlet">Task List</a>
<a class="task" href="task-register-transition-servlet">New Task</a>
<a class="logout" id="show-dialog">Logout</a>
</nav>
</div>
<dialog class="mdl-dialog">
								<h4 class="mdl-dialog__title">Are you sure you want to log out?</h4>
								<div class="mdl-dialog__actions">
									<form action="logout-servlet" method="POST">
										<button type="submit" class="mdl-button">OK</button>
									</form>
										<button type="button" class="mdl-button close">cancel</button>
									
								</div>
							</dialog>
							 <script>
							    var dialog = document.querySelector('dialog');
							    var showDialogButton = document.querySelector('#show-dialog');
							    if (! dialog.showModal) {
							      dialogPolyfill.registerDialog(dialog);
							    }
							    showDialogButton.addEventListener('click', function() {
							      dialog.showModal();
							    });
							    dialog.querySelector('.close').addEventListener('click', function() {
							      dialog.close();
							    });
							  </script>
<main class="mdl-layout__content">
<div class="page-content">
	<%
		int tableCount = 0;
		for (TaskBean task : tasks) {
		if(task.getStatus().getName().equals("Underway") || task.getStatus().getName().equals("Not started")){
			tableCount++;
		}
		}
	%>
<div class="area">
    <input type="radio" name="tab_name" id="tab1" checked>
    <label class="tab_class" for="tab1">Yet/Doing (<%=tableCount%>)</label>
    <div class="content_class">
      	    <%
	        for (TaskBean task : tasks) {
	    	if (task.getStatus().getName().equals("Not started") || task.getStatus().getName().equals("Underway")) {
	        %>
	        <ul class="tasks">
	        <li>
	        <span class="task"><a href="task-detail-servlet?task-id=<%=task.getTaskId() %>"><%=task.getTaskName() %></a></span>
	        <%
	        if (task.getStatus().getName().equals("Not started")) {
	        %>
	        <span class="not-start"><%=task.getStatus().getName() %></span><br>
	        <%
	        } else {
	        %>
	        <span class="in-start"><%=task.getStatus().getName() %></span><br>
	        <%
	        }
	        %>
	        <span class="userdate"><span class="limitdate"><img src="images/calendar.png" width="20" height="20" class="calendar-icon"><%=task.getLimitDate()%></span>
	        <span class="PIC"><img src="<%=task.getUser().getIconPath()%>" width="20" height="20" class="user-icon"><%=task.getUser().getUserName() %></span>
	        </span>
	        </li>
	        </ul>
	        <%
	    	}
	        }
	        %>
    </div>
    <input type="radio" name="tab_name" id="tab2" >
    <label class="tab_class" for="tab2">Completed (<%=tasks.size() - tableCount%>)</label>
    <div class="content_class">
           	    <%
	        for (TaskBean task : tasks) {
	    	if (task.getStatus().getName().equals("Completed")) {
	        %>
	        <ul class="tasks">
	        <li>
	        <span class="task"><a href="task-detail-servlet?task-id=<%=task.getTaskId() %>"><%=task.getTaskName() %></a></span>
	        <span class="completed"><%=task.getStatus().getName() %></span><br>
	        <span class="userdate"><span class="limitdate"><img src="images/calendar.png" width="20" height="20" class="calendar-icon"><%=task.getLimitDate()%></span>
	        <span class="PIC"><img src="<%=task.getUser().getIconPath()%>" width="20" height="20" class="user-icon"><%=task.getUser().getUserName() %></span>
	        </span>
	        </li>
	        </ul>
	        <%
	    	}
	        }
	        %>
    </div>
    <input type="radio" name="tab_name" id="tab3" >
    <label class="tab_class" for="tab3">All (<%=tasks.size() %>)</label>
    <div class="content_class">
           	    <%
	        for (TaskBean task : tasks) {
	    	if (task.getStatus().getName().equals("Underway") || task.getStatus().getName().equals("Not started") || task.getStatus().getName().equals("Completed")) {
	        %>
	        <ul class="tasks">
	        <li>
	        <span class="task"><a href="task-detail-servlet?task-id=<%=task.getTaskId() %>"><%=task.getTaskName() %></a></span>
	        <%
	        if (task.getStatus().getName().equals("Not started")) {
	        %>
	        <span class="not-start"><%=task.getStatus().getName() %></span><br>
	        <%
	        } else if (task.getStatus().getName().equals("Underway")) {
	        %>
	        <span class="in-start"><%=task.getStatus().getName() %></span><br>
	        <%
	        } else {
	        %>
	        <span class="completed"><%=task.getStatus().getName() %></span><br>
	        <%
	        }
	        %>
	        <span class="userdate"><span class="limitdate"><img src="images/calendar.png" width="20" height="20" class="calendar-icon"><%=task.getLimitDate()%></span>
	        <span class="PIC"><img src="<%=task.getUser().getIconPath()%>" width="20" height="20" class="user-icon"><%=task.getUser().getUserName() %></span>
	        </span>
	        </li>
	        </ul>
      <%
	   } 	
	   }
      %>
    </div>
  </div>
</div>
</main>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="task-list.js"></script>
</body>
</html>