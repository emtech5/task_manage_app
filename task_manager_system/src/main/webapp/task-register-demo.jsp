<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="model.entity.CategoryBean, model.entity.UserBean, model.entity.StatusBean, java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録</title>
</head>
<% 
	List<UserBean> userList = (List<UserBean>)request.getAttribute("userList");
	List<CategoryBean> categoryList = (List<CategoryBean>)request.getAttribute("categoryList");
	List<StatusBean> statusList = (List<StatusBean>)request.getAttribute("statusList");
%>
<body>
<form action="task-register-servlet" method="post">
タスク名：
<input type="text" name="taskName"><br>

カテゴリ：
<input class="js-select" type="radio" name="new_or_exist" value="exist" onclick="formSwitch()" checked>選択
<input class="js-select" type="radio" name="new_or_exist" value="new" onclick="formSwitch()">新規登録
  <span id = "selectExisting">
    <select id = "changeSelect" name = "categoryId">
    <% for(CategoryBean category : categoryList) { %>
      <option value=<%=category.getId() %>><%=category.getName() %></option>
     <%} %>
     </select>
  </span>
  <span id = "selectNew">
    <input type = "text" name = "categoryName">
  </span>
  
期限：
<input type="date" value="2222-02-22"><br>

担当者：
<select><option></select><br>

ステータス：
<select><option></select><br>

メモ：
<textarea></textarea><br>

<input type="submit" value="登録">
</form>

<script>
    var selectBox = document.getElementById('selectExisting');
    var text = document.getElementById('selectNew');
 
    selectBox.style.display = "none";
    text.style.display = "none";

    function formSwitch() {
        check = document.getElementsByClassName('js-select')
        if (check[0].checked) {
            selectBox.style.display = "block";
	    text.style.display = "none";

        } else if (check[1].checked) {
	    selectBox.style.display = "none";
	    text.style.display = "block";

        } else {
        }
    }
    window.addEventListener('load', formSwitch());

  </script>
</body>
</html>