<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<!-- Font Awesome -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
<!-- MDB -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.css" rel="stylesheet"/>
<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.js"></script>
<link rel = "stylesheet" href = "login.css">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.grey-pink.min.css" />
</head>

<body>
    <div class="card">
        <div class = "card-title">
		  <img src="images/room.jpg" class="card-img-top" alt="Fissure in Sandstone"/>
		  <p>Task Management System</p>
		</div>
		  <div class="card-body">
	      <form action = "login-servlet" method = "POST">
	        <div class="error-message">
		        <%request.setCharacterEncoding("UTF-8");
		    	Boolean isFailure = (Boolean)request.getAttribute("isFailure");
		    	if ( isFailure != null && isFailure ) {
		    	%>
		    	The value entered is invalid.
		    	<%
		    	}
		    	%>
    	    </div>   
    	    <div class = "text-boxs">
	            <input class = "textbox" type="text" name="userId" placeholder="userID" onEnter="send()" autocomplete="off">
	            <input class = "textbox" type="password" name="password" placeholder="password" onEnter="send()" autocomplete="off">
	            <br>
	            <div class="btn-btn-success">
					<button type = "submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
					    <div class = "login-font">Login</div>
					</button>
	            </div>
	        </div>
          </form>
      </div>
    </div>
    <script>
    function send() {
    	document.form.submit()
    	}
    </script>
  </body>
</html>