package model.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.entity.CategoryBean;
import model.entity.StatusBean;
import model.entity.TaskBean;
import model.entity.UserBean;

/**
 * t_taskテーブルのDAO
 * @author emtech-user
 *
 */
public class TaskDao {

	/**
	 * 指定されたタスクIDのタスクオブジェクトを返す
	 * @param taskID タスクID
	 * @return タスクオブジェクト
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalArgumentException 
	 */
	public TaskBean get(int taskId) throws IllegalArgumentException, ClassNotFoundException, SQLException {
		TaskBean task = new TaskBean();


		String sql = "SELECT * FROM t_task t1 LEFT OUTER JOIN m_category t2 ON t1.category_id = t2.category_id LEFT OUTER JOIN m_user t3 ON t1.user_id = t3.user_id LEFT OUTER JOIN m_status t4 ON t1.status_code = t4.status_code WHERE task_id = ?";
		
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)) {

			pstmt.setInt(1, taskId);

			ResultSet resultSet = pstmt.executeQuery();

			if (resultSet.next()) {
				task = new TaskBean(
						resultSet.getInt("task_id"), 
						resultSet.getString("task_name"),
						//カテゴリオブジェクトを生成,
						new CategoryBean(resultSet.getInt("category_id"),resultSet.getString("category_name")),
						resultSet.getDate("limit_date"),							
						//ユーザオブジェクトを生成,
						new UserBean(resultSet.getString("user_id"), resultSet.getString("password"), resultSet.getString("user_name"), resultSet.getString("icon_path")),
						//ステータスオブジェクトを生成,
						new StatusBean(resultSet.getString("status_code"), resultSet.getString("status_name")),
						resultSet.getString("memo"),
						resultSet.getTimestamp("create_datetime"),
						resultSet.getTimestamp("update_datetime")
						);
			} else {
				throw new IllegalArgumentException();
			}
			return task;

		}

	}

	/**
	 * 全てのタスク情報一覧を返す
	 * @return タスク情報一覧
	 * @throws SQLException 
	 * @throws IllegalArgumentException 
	 * @throws ClassNotFoundException 
	 */
	public List<TaskBean> getAll() throws IllegalArgumentException, SQLException, ClassNotFoundException {
		List<TaskBean> tasks = new ArrayList<TaskBean>();

		String sql = "SELECT * FROM t_task t1 LEFT OUTER JOIN m_category t2 ON t1.category_id = t2.category_id LEFT OUTER JOIN m_user t3 ON t1.user_id = t3.user_id LEFT OUTER JOIN m_status t4 ON t1.status_code = t4.status_code";
		try (Connection con = ConnectionManager.getConnection();
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			while (resultSet.next()) {
				tasks.add(
						new TaskBean(
								resultSet.getInt("task_id"), 
								resultSet.getString("task_name"),
								//カテゴリオブジェクトを生成,
								new CategoryBean(resultSet.getInt("category_id"),resultSet.getString("category_name")),
								resultSet.getDate("limit_date"),							
								//ユーザオブジェクトを生成,
								new UserBean(resultSet.getString("user_id"), resultSet.getString("password"), resultSet.getString("user_name"), resultSet.getString("icon_path")),
								//ステータスオブジェクトを生成,
								new StatusBean(resultSet.getString("status_code"), resultSet.getString("status_name")),
								resultSet.getString("memo"),
								resultSet.getTimestamp("create_datetime"),
								resultSet.getTimestamp("update_datetime")
								)
						);
			}
		}

		return tasks;
	}
	/**
	 * 指定されたタスク情報をt_taskに登録する
	 * @param task
	 * @return 登録件数
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public int add(TaskBean task) throws ClassNotFoundException, SQLException {
		int registerCount = 0;
		String sql = "INSERT INTO t_task (task_name, category_id, limit_date, user_id, status_code, memo)"
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)){

			String taskName = task.getTaskName();
			int categoryId = task.getCategory().getId();
			Date limitDate = task.getLimitDate();
			String userId = task.getUser().getUserId();
			String statusCode = task.getStatus().getCode();
			String memo = task.getMemo();				

			pstmt.setString(1, taskName);
			pstmt.setInt(2, categoryId);
			pstmt.setDate(3, limitDate);
			pstmt.setString(4, userId);
			pstmt.setString(5, statusCode);
			pstmt.setString(6, memo);

			registerCount = pstmt.executeUpdate();
		}
		return registerCount;
	}
	/**
	 * 指定されたタスク情報を更新
	 * @param task タスクオブジェクト
	 * @return 更新件数
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public int update(TaskBean task) throws SQLException, ClassNotFoundException {
		int updateCount = 0;
		String sql = "UPDATE t_task SET task_name=?, category_id=?, limit_date=?, user_id=?,status_code=?,memo=? WHERE task_id=?";
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)){

			int taskId = task.getTaskId();
			String taskName = task.getTaskName();
			int categoryId = task.getCategory().getId();
			Date limitDate = task.getLimitDate();
			String userId = task.getUser().getUserId();
			String statusCode = task.getStatus().getCode();
			String memo = task.getMemo();				

			pstmt.setString(1, taskName);
			pstmt.setInt(2, categoryId);
			pstmt.setDate(3, limitDate);
			pstmt.setString(4, userId);
			pstmt.setString(5, statusCode);
			pstmt.setString(6, memo);
			pstmt.setInt(7, taskId);

			updateCount = pstmt.executeUpdate();
		}
		return updateCount;
	}
	/**
	 * 指定されたタスクIDのレコードを削除
	 * @param taskID タスクID
	 * @return 削除件数
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int delete(int taskId) throws ClassNotFoundException, SQLException {

		int processingNumber = 0;

		String sql = "DELETE FROM t_task WHERE task_id = ?";

		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)) {

			pstmt.setInt(1, taskId);

			processingNumber = pstmt.executeUpdate();
		}
		return processingNumber;
	}
	
	/**
	 * 指定された文字列でタスク名にあいまい検索を行って、タスクの一覧を返す
	 * @param taskName 検索文字列
	 * @return タスクの一覧
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public List<TaskBean> fuzzySearch(String taskName) throws IllegalArgumentException, SQLException, ClassNotFoundException {
		List<TaskBean> tasks = new ArrayList<TaskBean>();

		String sql = "SELECT * FROM t_task t1 LEFT OUTER JOIN m_category t2 ON t1.category_id = t2.category_id LEFT OUTER JOIN m_user t3 ON t1.user_id = t3.user_id LEFT OUTER JOIN m_status t4 ON t1.status_code = t4.status_code WHERE t1.task_name LIKE ?";
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)) {
			
			pstmt.setString(1, "%" + taskName + "%");
			
			ResultSet resultSet = pstmt.executeQuery();
			
			while (resultSet.next()) {
				tasks.add(
						new TaskBean(
								resultSet.getInt("task_id"), 
								resultSet.getString("task_name"),
								//カテゴリオブジェクトを生成,
								new CategoryBean(resultSet.getInt("category_id"),resultSet.getString("category_name")),
								resultSet.getDate("limit_date"),							
								//ユーザオブジェクトを生成,
								new UserBean(resultSet.getString("user_id"), resultSet.getString("password"), resultSet.getString("user_name"), resultSet.getString("icon_path")),
								//ステータスオブジェクトを生成,
								new StatusBean(resultSet.getString("status_code"), resultSet.getString("status_name")),
								resultSet.getString("memo"),
								resultSet.getTimestamp("create_datetime"),
								resultSet.getTimestamp("update_datetime")
								)
						);
			}
		}

		return tasks;
	}

}
