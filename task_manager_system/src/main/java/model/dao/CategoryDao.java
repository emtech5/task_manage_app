package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.entity.CategoryBean;
import model.exception.DuplicationExpection;

public class CategoryDao {

	// カテゴリーを追加するメソッド
	public int add(CategoryBean category) throws ClassNotFoundException, SQLException {
		// 更新カウント
		int insertionCount = 0;
		String sql = "INSERT INTO m_category (category_name) VALUES (?)";
		
		// DB接続
		try(Connection connection = ConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			// プレースホルダに代入
			statement.setString(1, category.getName());
			// sqlの実行
			insertionCount = statement.executeUpdate();
		}
		return insertionCount;
	}
	
	// すべてのカテゴリを取得するメソッド
	public List<CategoryBean> getAll() throws ClassNotFoundException, SQLException {
		
		// リストの作成
		List<CategoryBean> category = new ArrayList<CategoryBean>();
		
		String sql = "SELECT * FROM m_category";
		// DB接続
		try (Connection con = ConnectionManager.getConnection();
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			while (resultSet.next()) {
				category.add(new CategoryBean(resultSet.getInt("category_id"),
						resultSet.getString("category_name")));
			}
		}
		return category;
	}
	
	// 指定されたIDからカテゴリを探すメソッド
	public CategoryBean get(int categoryId) throws ClassNotFoundException, SQLException, IllegalArgumentException {
		
		String sql = "SELECT category_name FROM m_category WHERE category_id = ? ";
		
		try(Connection connection = ConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			// プレースホルダに代入
			statement.setInt(1, categoryId);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				CategoryBean category = new CategoryBean(categoryId, resultSet.getString("category_name"));
				return category;
			} else {
				throw new IllegalArgumentException("指定されたIDのカテゴリは存在しません");
			}
		}
	}
	
	public int getCategoryId(String name) throws ClassNotFoundException, SQLException, IllegalArgumentException {
		
		String sql = "SELECT category_id FROM m_category WHERE category_name = ? ";
		
		try(Connection connection = ConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			// プレースホルダに代入
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				int categoryId = resultSet.getInt("category_id");
				return categoryId;
			} else {
				throw new IllegalArgumentException("指定された名前のカテゴリは存在しません");
			}
		}
	}
	
	public void check(String name) throws ClassNotFoundException, SQLException, DuplicationExpection {
		
		String sql = "SELECT * FROM m_category WHERE category_name = ? ";
		
		try(Connection connection = ConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			// プレースホルダに代入
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				throw new DuplicationExpection("入力されたカテゴリはすでに登録されています。");
			}
		}
	}
	
	public int delete(int categoryId) throws ClassNotFoundException, SQLException {
		// 更新カウント
		int deleteCount = 0;
		String sql = "DELETE FROM m_category WHERE category_id = ?";
		
		// DB接続
		try(Connection connection = ConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			// プレースホルダに代入
			statement.setInt(1, categoryId);
			// sqlの実行
			deleteCount = statement.executeUpdate();
		}
		return deleteCount;
	}
	
}
