package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.entity.StatusBean;

public class StatusDao {
	
	// すべてのステータスを取得するメソッド
	public List<StatusBean> getAll() throws ClassNotFoundException, SQLException {
		
		List<StatusBean> statuses = new ArrayList<StatusBean>();
		
		String sql = "SELECT * FROM m_status";
		
		try (Connection con = ConnectionManager.getConnection();
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			while (resultSet.next()) {
				statuses.add(new StatusBean(resultSet.getString("status_code"),
						resultSet.getString("status_name")));
			}
		}
		return statuses;
	}
	
	// 指定されたコードからステータスを探すメソッド
	public StatusBean get(String statusCode) throws ClassNotFoundException, SQLException, IllegalArgumentException {
		
		String sql = "SELECT status_name FROM m_status WHERE status_code = ? ";
		
		try(Connection connection = ConnectionManager.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			// プレースホルダに代入
			statement.setString(1, statusCode);
			ResultSet resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				StatusBean status = new StatusBean(statusCode, resultSet.getString("status_name"));
				return status;
			} else {
				throw new IllegalArgumentException("指定されたコードのステータスは存在しません");
			}
		}
	}

}
