package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.entity.CommentBean;
import model.entity.UserBean;

public class CommentDao {
	
	public List<CommentBean> search(int taskId) throws SQLException, ClassNotFoundException {
		List<CommentBean> comments = new ArrayList<>();
		UserDao userDAO = new UserDao();
		
		String sql = "SELECT * FROM t_comment WHERE task_id=?";
		
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)){
			
			pstmt.setInt(1, taskId);
			ResultSet res = pstmt.executeQuery();
			
			while(res.next()) {
				String commentId = res.getString("comment_id");
				String userId = res.getString("user_id");
				String comment = res.getString("comment");
				Timestamp updateDatetime = res.getTimestamp("update_datetime");
				UserBean user = userDAO.get(userId);
				comments.add(new CommentBean(commentId, taskId, user, comment, updateDatetime));
			}
		}
		return comments;
	}
	
	public int add(CommentBean newComment) throws SQLException, ClassNotFoundException {
		
		int addNumber = 0;
		String sql = "INSERT INTO t_comment (task_id, user_id, comment, update_datetime) VALUES(?, ?, ?, ?)";
		
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)){
			
			int taskId = newComment.getTaskId();
			String userId = newComment.getUser().getUserId();
			String comment = newComment.getComment();
			Timestamp updateDatetime = newComment.getUpdateDatetime();
			
			pstmt.setInt(1, taskId);
			pstmt.setString(2, userId);
			pstmt.setString(3, comment);
			pstmt.setTimestamp(4, updateDatetime);
			
			addNumber = pstmt.executeUpdate();
		}
		return addNumber;
	}
	public int delete(int taskId) throws SQLException, ClassNotFoundException {
		
		int deleteNumber = 0;
		String sql = "DELETE FROM t_comment WHERE task_id=?";
		
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)){
			
			pstmt.setInt(1, taskId);
			
			deleteNumber = pstmt.executeUpdate();
		}
		return deleteNumber;
	}
}
