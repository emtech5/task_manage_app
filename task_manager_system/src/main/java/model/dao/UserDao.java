package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.entity.UserBean;

public class UserDao {
	/**
	 * 
	 * @param user
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public UserBean login(String userId, String password) throws SQLException, ClassNotFoundException, IllegalArgumentException{
		
		UserBean user = new UserBean();
		
		String sql = "SELECT * FROM m_user WHERE user_id = ? AND password = ?";
		
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)) {
			
			pstmt.setString(1, userId);
			pstmt.setString(2, password);
			
			ResultSet res = pstmt.executeQuery();
			
			if (res.next()) {
				String userName = res.getString("user_name");
				String iconPath = res.getString("icon_path");
				user = new UserBean(userId, password, userName, iconPath);
				
			} else {
				throw new IllegalArgumentException("入力された値が不正です。");
			} 
		}
		return user;
	}
	
	public List<UserBean> getAll() throws SQLException, ClassNotFoundException {
		List<UserBean> users = new ArrayList<>();
		
		String sql = "SELECT user_id, user_name, icon_path FROM m_user";
				
		try (Connection con = ConnectionManager.getConnection();
				Statement stmt = con.createStatement();
				ResultSet res = stmt.executeQuery(sql)){
			
			while(res.next()) {
				String userId = res.getString("user_id");
				String userName = res.getString("user_name");
				String iconPath = res.getString("icon_path");
				users.add(new UserBean(userId, "", userName, iconPath));
			}
		}
		return users;
	}
	
	public UserBean get(String userId) throws SQLException, ClassNotFoundException {
		UserBean user = new UserBean();
		
		String sql = "SELECT user_id, user_name, icon_path FROM m_user WHERE user_id=?";
		
		try (Connection con = ConnectionManager.getConnection();
				PreparedStatement pstmt = con.prepareStatement(sql)){
			
			pstmt.setString(1, userId);
			ResultSet res = pstmt.executeQuery();
			
			if (res.next()) {
				String userName = res.getString("user_name");
				String iconPath = res.getString("icon_path");
				user = new UserBean(userId, "", userName, iconPath);
			}
		}
		return user;
	}
}
