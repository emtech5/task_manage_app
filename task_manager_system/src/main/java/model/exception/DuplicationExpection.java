package model.exception;

public class DuplicationExpection extends Exception {

	public DuplicationExpection(String message) {
		super(message);
	}
}
