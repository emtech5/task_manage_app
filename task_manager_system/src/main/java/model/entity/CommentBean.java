package model.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * コメントを表す
 * comment_dbのDTOでありBeanオブジェクトです。
 *
 */
public class CommentBean implements Serializable {
	private final int MAX_COMMENT_LENGTH = 100;
	
	/**
	 * コメントID
	 */
	private String commentId;
	
	/**
	 * タスクID
	 */
	private int taskId;
	
	/**
	 * ユーザID
	 */
	private UserBean user;
	
	/**
	 * コメント
	 */
	private String comment;
	
	/**
	 * 更新日時
	 */
	private Timestamp updateDatetime;
	
	@Deprecated
	public CommentBean() {
	}

	public CommentBean(String commentId, int taskId, UserBean user, String comment, Timestamp updateDatetime) throws IllegalArgumentException {
		if (Objects.isNull(commentId)) {
			throw new IllegalArgumentException("コメントIDにnullは指定できません");
		}
		if (Objects.isNull(taskId)) {
			throw new IllegalArgumentException("タスクIDにnullは指定できません");
		}
		if (Objects.isNull(user)) {
			throw new IllegalArgumentException("ユーザにnullは指定できません");
		}
		if (Objects.isNull(comment) || comment.length() > MAX_COMMENT_LENGTH) {
			throw new IllegalArgumentException("コメントは100文字以内を設定してください");
		}
		if (Objects.isNull(updateDatetime)) {
			throw new IllegalArgumentException("更新日時にnullは指定できません");
		}
		this.commentId = commentId;
		this.taskId = taskId;
		this.user = user;
		this.comment = comment;
		this.updateDatetime = updateDatetime;
	}
	
	/**
	 * フィールドcommentIDの値を返します
	 * @return
	 */
	public String getCommentId() {
		return commentId;
	}
	
	/**
	 * フィールドcommentIDの値を設定します
	 * @param commentID
	 * @throws IllegalArgumentException
	 */
	@Deprecated
	public void setCommentId(String commentId) throws IllegalArgumentException {
		if (Objects.isNull(commentId)) {
			throw new IllegalArgumentException("コメントIDにnullは指定できません");
		}
		this.commentId = commentId;
	}
	
	/**
	 * フィールドtaskID値を返します
	 * @return
	 */
	public int getTaskId() {
		return taskId;
	}
	
	/**
	 * フィールドtaskIDの値を設定します
	 * @param taskID
	 * @throws IllegalArgumentException
	 */
	@Deprecated
	public void setTaskId(int taskId) throws IllegalArgumentException {
		if (Objects.isNull(taskId)) {
			throw new IllegalArgumentException("タスクIDにnullは指定できません");
		}
		this.taskId = taskId;
	}
	
	/**
	 * フィールドuserIDの値を返します
	 * @return
	 */
	public UserBean getUser() {
		return user;
	}
	
	/**
	 * フィールドuserIDの値を設定します
	 * @param userID
	 * @throws IllegalArgumentException
	 */
	@Deprecated
	public void setUser(UserBean user) throws IllegalArgumentException {
		if (Objects.isNull(user)) {
			throw new IllegalArgumentException("ユーザにnullは指定できません");
		}
		this.user = user;
	}
	
	/**
	 * フィールドcommentの値を返します
	 * @return
	 */
	public String getComment() {
		return comment;
	}
	
	/**
	 * フィールドcommentの値を設定します
	 * @param comment
	 * @throws IllegalArgumentException
	 */
	@Deprecated
	public void setComment(String comment) throws IllegalArgumentException {
		if (Objects.isNull(comment) || comment.length() > MAX_COMMENT_LENGTH) {
			throw new IllegalArgumentException("コメントは100文字以内を設定してください");
		}
		this.comment = comment;
	}
	
	/**
	 * フィールドupdate_datetimeの値を返します
	 * @return
	 */
	public Timestamp getUpdateDatetime() {
		return updateDatetime;
	}
	
	/**
	 * フィールドupdate_datetimeの値を設定します
	 * @param updateDatetime
	 * @throws IllegalArgumentException
	 */
	@Deprecated
	public void setUpdateDatetime(Timestamp updateDatetime) throws IllegalArgumentException {
		if (Objects.isNull(updateDatetime)) {
			throw new IllegalArgumentException("更新日時にnullは指定できません");
		}
		this.updateDatetime = updateDatetime;
	}
	
	
}
