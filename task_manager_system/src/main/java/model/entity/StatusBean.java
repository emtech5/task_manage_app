package model.entity;

import java.util.Objects;

public class StatusBean {
	
	private final int MAX_CODE = 3;
	
	private final int MIN_CODE = 0;
	
	private final int MAX_NAME_LENGTH = 20;
	
	private final int MIN_NAME_LENGTH = 0;
	/**
	 * ステータスコード
	 */
	private String code;
	
	/**
	 * ステータス名
	 */
	private String name;
	
	@Deprecated
	public StatusBean() {
		
	}
	
	public StatusBean(String code, String name) throws IllegalArgumentException {
		if(Objects.isNull(code) || code.equals("") || code.length() > MAX_CODE || code.length() <= MIN_CODE) {
			throw new IllegalArgumentException("ステータスコードにNULLは設定できません");
		}
		if(code.equals("")) {
			throw new IllegalArgumentException("ステータスコードに値がありません");
		}
		if(code.length() > MAX_CODE || code.length() <= MIN_CODE) {
			throw new IllegalArgumentException("ステータスコードの文字数が不正です");
		}
		if(Objects.isNull(name)) {
			throw new IllegalArgumentException("ステータス名にNULLは設定できません");
		}
		if(name.equals("")) {
			throw new IllegalArgumentException("ステータス名に値がありません");
		}
		if(name.length() > MAX_NAME_LENGTH || name.length() <= MIN_NAME_LENGTH) {
			throw new IllegalArgumentException("ステータス名の文字数が不正です");
		}
		this.code = code;
		this.name = name;
	}
	

	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code セットする code
	 */
	@Deprecated
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	@Deprecated
	public void setName(String name) {
		this.name = name;
	}
	
	
}
