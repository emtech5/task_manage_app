package model.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
/**
 * タスクを表す
 * @author emtech-user
 *
 */
public class TaskBean implements Serializable{
	private static int MAX_TASK_NAME = 50;
	private static int MIN_TASK_NAME = 0;
	/**
	 * タスクID
	 */
	private int taskId;
	/**
	 * タスク名
	 */
	private String taskName;
	/**
	 * カテゴリ
	 */
	private CategoryBean category;
	/**
	 * 期限
	 */
	private Date limitDate;
	/**
	 * ユーザ
	 */
	private UserBean user;
	/**
	 * ステータス
	 */
	private StatusBean status;
	/**
	 * メモ
	 */
	private String memo;
	/**
	 * 登録日時
	 */
	private Timestamp registrationTime;
	/**
	 * 更新日時
	 */
	private Timestamp updateTime;
	
	// CommentBeanオブジェクト作成前のためコメント化
	/**
	 * コメントリスト
	 */
// private List<CommentBean> comments;  
	/**
	 * 引数無しコンストラクタ
	 */
	@Deprecated
	public TaskBean() {
	}
	/**
	 * 指定された値を持つタスクオブジェクトを生成する
	 * @param taskId
	 * @param taskName
	 * @param category
	 * @param limitDate
	 * @param user
	 * @param status
	 * @param timestamp
	 * @param timestamp2
	 */
	public TaskBean(int taskId, String taskName, CategoryBean category, Date limitDate, UserBean user, StatusBean status, String memo, Timestamp registrationTime, Timestamp updateTime) {
		if(taskName.length() >= MAX_TASK_NAME) {
			throw new IllegalArgumentException("タスク名は" + MAX_TASK_NAME + "文字以下です");
		}
		if(taskName.length() <= MIN_TASK_NAME) {
			throw new IllegalArgumentException("タスク名は" + MIN_TASK_NAME + "文字以上です");
		}
		this.taskId = taskId;
		this.taskName = taskName;
		this.category = category;
		this.limitDate = limitDate;
		this.user = user;
		this.status = status;
		this.memo = memo;
		this.registrationTime = registrationTime;
		this.updateTime = updateTime;
	}

	/**
	 * @return taskId
	 */
	public int getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId セットする taskId
	 */
	@Deprecated
	public void setTaskID(int taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return taskName
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @param taskName セットする taskName
	 */
	@Deprecated
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * @return category
	 */
	public CategoryBean getCategory() {
		return category;
	}

	/**
	 * @param category セットする category
	 */
	@Deprecated
	public void setCategory(CategoryBean category) {
		this.category = category;
	}

	/**
	 * @return limitDate
	 */
	public Date getLimitDate() {
		return limitDate;
	}
	/**
	 * @param limitDate セットする limitDate
	 */
	@Deprecated
	public void setLimitDate(Date limitDate) {
		this.limitDate = limitDate;
	}
	/**
	 * @return user
	 */
	public UserBean getUser() {
		return user;
	}
	/**
	 * @param user セットする user
	 */
	@Deprecated
	public void setUser(UserBean user) {
		this.user = user;
	}

	/**
	 * @return status
	 */
	public StatusBean getStatus() {
		return status;
	}

	/**
	 * @param status セットする status
	 */
	@Deprecated
	public void setStatus(StatusBean status) {
		this.status = status;
	}

	/**
	 * @return memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * @param memo セットする memo
	 */
	@Deprecated
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * @return registrationTime
	 */
	public Timestamp getRegistrationTime() {
		return registrationTime;
	}

	/**
	 * @param registrationTime セットする registrationTime
	 */
	@Deprecated
	public void setRegistrationTime(Timestamp registrationTime) {
		this.registrationTime = registrationTime;
	}

	/**
	 * @return updateTime
	 */
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime セットする updateTime
	 */
	@Deprecated
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	
	// CommentBeanオブジェクト作成前のためコメント化
//	/**
//	 * @return comments
//	 */
//	public List<CommentBean> getComments() {
//		return comments;
//	}
//
//	/**
//	 * @param comments セットする comments
//	 */
//	public void setComments(List<CommentBean> comments) {
//		this.comments = comments;
//	}
	
	
}
