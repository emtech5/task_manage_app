package model.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * ユーザを表す
 * user_dbのDTOでありBeanオブジェクトです。
 * @author emtech-user
 *
 */
public class UserBean implements Serializable {
	private final int MAX_userId_LENGTH = 24;
	private final int MAX_PASSWORD_LENGTH = 32;
	private final int MAX_USERNAME_LENGTH = 20;
	private final int MAX_ICONPATH_LENGTH = 100;
	
	/**
	 * ユーザーID
	 */
	private String userId;
	
	/**
	 * パスワード
	 */
	private String password;
	
	/**
	 * ユーザ名
	 */
	private String userName;
	
	/**
	 * アイコンのパス
	 */
	private String iconPath;
	
	@Deprecated
	public UserBean() {
		
	}

	public UserBean(String userId, String password, String userName, String iconPath) throws IllegalArgumentException {
		if (Objects.isNull(userId) || userId.length() > MAX_userId_LENGTH) {
			throw new IllegalArgumentException();
		}
		if (Objects.isNull(password) || password.length() > MAX_PASSWORD_LENGTH) {
			throw new IllegalArgumentException();
		}
		if (Objects.isNull(userName) || userName.length() > MAX_USERNAME_LENGTH) {
			throw new IllegalArgumentException();
		}
		if (Objects.isNull(iconPath) || userName.length() > MAX_ICONPATH_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.userId = userId;
		this.password = password;
		this.userName = userName;
		this.iconPath = iconPath;
	}
	
	/**
	 * フィールドuserIdの値を返します
	 * @return
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * フィールドuserIdの値を設定します
	 * @param userId
	 */
	@Deprecated
	public void setUserId(String userId) throws IllegalArgumentException {
		if (Objects.isNull(userId) || userId.length() > MAX_userId_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.userId = userId;
	}
	
	/**
	 * フィールドpasswordの値を返します
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * フィールドpasswordの値を設定します
	 * @param password
	 */
	@Deprecated
	public void setPassword(String password) throws IllegalArgumentException {
		if (Objects.isNull(password) || password.length() > MAX_PASSWORD_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.password = password;
	}
	
	/**
	 * フィールドuserNameの値を返します
	 * @return
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * フィールドuserNameの値を設定します
	 * @param userName
	 */
	@Deprecated
	public void setUserName(String userName) throws IllegalArgumentException {
		if (Objects.isNull(userName) || userName.length() > MAX_USERNAME_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.userName = userName;
	}
	
	/**
	 * フィールドiconPathの値を返します
	 * @return
	 */
	public String getIconPath() {
		return iconPath;
	}
	
	/**
	 * フィールドiconPathの値を設定します
	 * @param iconPath
	 */
	@Deprecated
	public void setIconPath(String iconPath) throws IllegalArgumentException {
		this.iconPath = iconPath;
	}

	
}