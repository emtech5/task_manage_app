package model.entity;

import java.util.Objects;

public class CategoryBean {
	
	private final int MAX_NAME_LENGTH = 20;
	
	private final int MIN_NAME_LENGTH = 0;
	
	private final int MIN_CODE = 0;
	
	/**
	 * カテゴリID
	 */
	private int id;
	
	/**
	 * カテゴリ名
	 */
	private String name;
	
	@Deprecated
	public CategoryBean() {
		
	}
	/**
	 * 
	 * @param name
	 * @throws IllegalArgumentException
	 */
	public CategoryBean(int id, String name) throws IllegalArgumentException {
		if (Objects.isNull(id)) {
			throw new IllegalArgumentException("カテゴリーIDにNULLは設定できません");
		}
		if (id < MIN_CODE || id % 1 != 0) {
			throw new IllegalArgumentException("カテゴリーIDは正の整数を設定してください");
		}
		if (Objects.isNull(name) || name.equals("") || name.length() > MAX_NAME_LENGTH || name.length() <= MIN_NAME_LENGTH) {
			throw new IllegalArgumentException("カテゴリ名にNULLは設定できません");
		}
		if (name.equals("")) {
			throw new IllegalArgumentException("カテゴリ名に値がありません");
		}
		if (name.length() > MAX_NAME_LENGTH || name.length() <= MIN_NAME_LENGTH) {
			throw new IllegalArgumentException("カテゴリ名の文字数が不正です");
		}
		this.id = id;
		this.name = name;
	}
	/**
	 * @return code
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param code セットする code
	 */
	@Deprecated
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	@Deprecated
	public void setName(String name) {
		this.name = name;
	}

}
