package hello;

import com.slack.api.Slack;
import com.slack.api.webhook.Payload;
import com.slack.api.webhook.WebhookResponse;

public class DeleteNotification {
	public void sendMessage(int taskId, String taskName) throws Exception {
	  Slack slack = Slack.getInstance();
	
	  String webhookUrl = "https://hooks.slack.com/services/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"; 
	  Payload payload = Payload.builder().text("TaskID:" + taskId + " " + taskName + " has been Deleted!!").build();
	  System.out.println(webhookUrl);
	  WebhookResponse response = slack.send(webhookUrl, payload);
	  System.out.println(response); // WebhookResponse(code=200, message=OK, body=ok)
  }

}
