package servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hello.EditNotification;
import model.dao.CategoryDao;
import model.dao.StatusDao;
import model.dao.TaskDao;
import model.dao.UserDao;
import model.entity.CategoryBean;
import model.entity.StatusBean;
import model.entity.TaskBean;
import model.entity.UserBean;
import model.exception.DuplicationExpection;


/**
 * Servlet implementation class TaskEditServlet
 */
@WebServlet("/task-edit-servlet")
public class TaskEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		CategoryDao categoryDao = new CategoryDao();
		StatusDao statusDao = new StatusDao();
		UserDao userDao = new UserDao();
		TaskDao taskDao = new TaskDao();
		
		// リクエストパラメーターからタスクIDを取得
		int taskId = Integer.parseInt(request.getParameter("taskId"));
		
		// リクエストパラメーターからタスク名を取得
		String taskName = request.getParameter("taskName");
		System.out.println("taskName:" + taskName);
		// タスク名が空の場合エラーメッセージに「何か入力してくれ」のメッセージを付加する
		if ( "".equals(taskName) ) {
			String errorMessage = (String) request.getAttribute("errorMessage");
			if ( errorMessage == null || errorMessage.isEmpty() ) {
				request.setAttribute("errorMessage", "Please enter something in the task name.<br>");
				
			} else {
				request.setAttribute("errorMessage", 
					errorMessage + "Please enter something in the task name.<br>");
				
			}
		}
		
		// リクエストパラメーターから新規か選択かのパラメータを取得
		String newOrExist = request.getParameter("new_or_exist");
		
		// リクエストパラメーターから既存のカテゴリIDを取得
		int existCategoryId = Integer.parseInt(request.getParameter("existCategoryId"));
		
		// リクエストパラメーターから新しいカテゴリー名を取得
		String newCategoryName = request.getParameter("newCategoryName");
		// カテゴリ名が空の場合はエラーメッセージに「何か入力してくれ」のメッセージを付加
		
		
		// リクエストパラメーターから期限日時を取得
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date limitDate =  Date.valueOf((request.getParameter("limitDate")));
		
		// リクエストパラメーターからユーザーIDを取得
		String userId = request.getParameter("user_id");
		
		// リクエストパラメーターからステータスIDを取得
		String statusId = request.getParameter("status_id");
		
		// リクエストパラメーターからメモIDを取得
		String memo = request.getParameter("editMemo");
		
		int categoryId = 0;
		int updateCount = 0;
		

		try {
			
			// 既に登録済みのカテゴリ名を入力した場合をcatch
			if ( newOrExist.equals("new") ) {
				
				if ( "".equals(newCategoryName) ) {
					String errorMessage = (String) request.getAttribute("errorMessage");
					if ( errorMessage == null || errorMessage.isEmpty() ) {
						request.setAttribute("errorMessage", 
								"Please enter something in the category name.<br>");
					
					} else {
						request.setAttribute("errorMessage", 
							(String) request.getAttribute("errorMessage") 
							+ "Please enter something in the category name.<br>");
						
					}
						
				}
				
				try {
					categoryDao.check(newCategoryName);
					
				} catch ( DuplicationExpection e ) {
					
					String errorMessage = (String) request.getAttribute("errorMessage");
					if ( errorMessage == null || errorMessage.isEmpty() ) {
						request.setAttribute("errorMessage", "The input task is already exist.<br>");
					
					} else {
						request.setAttribute("errorMessage", "The input task is already exist.<br>" 
							+ (String) request.getAttribute("errorMessage"));
						
					}
					
					throw e;
				}
				CategoryBean category = new CategoryBean(0, newCategoryName);
				categoryDao.add(category);
				categoryId = categoryDao.getCategoryId(taskName);
					
			} else {
				categoryId = existCategoryId;
				
			}
			
			UserBean user = userDao.get(userId);
			CategoryBean category = categoryDao.get(categoryId);
			StatusBean status = statusDao.get(statusId);
			
			updateCount = taskDao.update(
					new TaskBean(taskId,
							taskName,
							category,
							limitDate,
							user,
							status,
							memo,
							null, 
							null
							)
					
					);
			
			System.out.println("statusId:" + statusId);
			if ( statusId.equals("99") ) {
				EditNotification editNotification = new EditNotification();
				try {
					editNotification.sendMessage(taskId, taskName);
				} catch (Exception e1) {
					// TODO 自動生成された catch ブロック
					e1.printStackTrace();
				}
				
			}
			
			// リクエストをタスク一覧画面.jspに転送
			RequestDispatcher rd = request.getRequestDispatcher("task-list-servlet");
			rd.forward(request, response);
			
			
		// 編集した項目をタスクに登録できなかった場合の例外をキャッチ
		} catch (IllegalArgumentException | ClassNotFoundException | SQLException | DuplicationExpection e) {
			
			request.setAttribute("taskId", taskId);
			request.setAttribute("errorChecker", "error");
			String errorMessage = (String) request.getAttribute("errorMessage");
			if ( errorMessage == null || errorMessage.isEmpty() ) {
				request.setAttribute("errorMessage", "We could not register.<br>");
				
			} else {
				request.setAttribute("errorMessage", "We could not register.<br>" 
				+ (String) request.getAttribute("errorMessage"));
			}
			
			
			RequestDispatcher requestDispatcher = 
					request.getRequestDispatcher("task-edit-transition-servlet");
			requestDispatcher.forward(request, response);
			
			e.printStackTrace();
			
		}
		
		
	}

}
