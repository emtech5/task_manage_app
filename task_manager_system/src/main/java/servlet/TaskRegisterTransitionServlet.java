package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.CategoryDao;
import model.dao.StatusDao;
import model.dao.UserDao;
import model.entity.CategoryBean;
import model.entity.StatusBean;
import model.entity.UserBean;

/**
 * Servlet implementation class TaskRegisterTransition
 */
@WebServlet("/task-register-transition-servlet")
public class TaskRegisterTransitionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskRegisterTransitionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
		// DAOをインスタンス化
		CategoryDao categoryDao = new CategoryDao();
		StatusDao statusDao = new StatusDao();
		UserDao userDao = new UserDao();
		
		// カテゴリー、ステータス、ユーザーオブジェクトのリスト
		List<CategoryBean> categoryList = new ArrayList<CategoryBean>();
		List<StatusBean> statusList = new ArrayList<StatusBean>();
		List<UserBean> userList = new ArrayList<UserBean>();
		
		try {
			// 各DAOでDBから取得したカテゴリ、ステータス、ユーザー
			// 全てを各リストに格納
			categoryList = categoryDao.getAll();
			statusList = statusDao.getAll();
			userList = userDao.getAll();
			
			// リクエストスコープへリストを設定
			request.setAttribute("categoryList", categoryList);
			request.setAttribute("statusList", statusList);
			request.setAttribute("userList", userList);
			// リクエストをタスク登録画面jspに転送
			RequestDispatcher rd = request.getRequestDispatcher("task-register.jsp");
			rd.forward(request, response);
		} catch (IllegalArgumentException | ClassNotFoundException | SQLException e) {
			RequestDispatcher rd = request.getRequestDispatcher("task-list-servlet");
			rd.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
