package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hello.DeleteNotification;
import model.dao.CommentDao;
import model.dao.TaskDao;
import model.entity.TaskBean;

/**
 * Servlet implementation class TaskDeleteServlet
 */
@WebServlet("/task-delete-servlet")
public class TaskDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TaskDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		

		// リクエストオブジェクトのエンコーディング方式の指定
		request.setCharacterEncoding("UTF-8");


		try { 
			int taskId = Integer.parseInt(request.getParameter("taskId"));	

			// DAOインスタンス化
			CommentDao commentDao = new CommentDao();
			TaskDao taskDao = new TaskDao();

			DeleteNotification deleteNotification = new DeleteNotification();
			try {
				TaskBean taskBean = new TaskBean();
				deleteNotification.sendMessage(taskId, taskBean.getTaskName());
			} catch (Exception e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
			
			try {
				commentDao.delete(taskId);
				taskDao.delete(taskId);

			} catch ( ClassNotFoundException | SQLException e ) {
				e.printStackTrace();
			}

			
			// タスク一覧にフォワード
			RequestDispatcher requestDispatcher = 
					request.getRequestDispatcher("task-list-servlet");
			requestDispatcher.forward(request, response);
			

		} catch ( IllegalArgumentException e ) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-detail.jsp");
			requestDispatcher.forward(request, response);

		}
	}

}
