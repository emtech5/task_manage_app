package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.CommentDao;
import model.entity.CommentBean;
import model.entity.UserBean;

/**
 * Servlet implementation class commentServlet
 */
@WebServlet("/comment-servlet")
public class commentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public commentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
		List<CommentBean> comments = new ArrayList<CommentBean>();
		CommentDao commentDao = new CommentDao();
		
		try {
			String comment = request.getParameter("comment");
			
			int taskId = Integer.parseInt(request.getParameter("task-id"));
			
			UserBean user = (UserBean) session.getAttribute("user");
			
			Timestamp updateDatetime = new Timestamp(System.currentTimeMillis());
			
			commentDao.add(new CommentBean("", taskId, user, comment, updateDatetime));
			
			comments = commentDao.search(taskId);
			
			request.setAttribute("comments", comments);
			request.setAttribute("isFailure", false);
			
		} catch (SQLException | ClassNotFoundException | IllegalArgumentException e) {
			request.setAttribute("isFailure", true);
			request.setAttribute("errorMessage", e.getMessage());
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-detail-servlet");
			requestDispatcher.forward(request, response);
		}
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-detail-servlet");
		requestDispatcher.forward(request, response);
	}
}