package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.CategoryDao;
import model.dao.StatusDao;
import model.dao.TaskDao;
import model.dao.UserDao;
import model.entity.CategoryBean;
import model.entity.StatusBean;
import model.entity.TaskBean;
import model.entity.UserBean;

/**
 * Servlet implementation class TaskSearchServlet
 */
@WebServlet("/task-search-servlet")
public class TaskSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
		List<TaskBean> tasks = new ArrayList<TaskBean>();
		TaskDao taskDao = new TaskDao();
		
		List<CategoryBean> categories = new ArrayList<CategoryBean>();
		CategoryDao categoryDao = new CategoryDao();
		
		List<StatusBean> statuses = new ArrayList<StatusBean>();
		StatusDao statusDao = new StatusDao();
		
		List<UserBean> users = new ArrayList<UserBean>();
		UserDao userDao = new UserDao();
		
		List<TaskBean> filterTask = new ArrayList<TaskBean>();
		
		request.setAttribute("search", request.getParameter("task-name"));
		
	 try {
			CategoryBean category = (CategoryBean)session.getAttribute("category");
			String taskName = request.getParameter("task-name");
			tasks = taskDao.fuzzySearch(taskName);
			if (category.getId() != 0) {
			    for (TaskBean task : tasks) {
			    	if(task.getCategory().getId() == category.getId()) {
			    		filterTask.add(task);
			    	}
			    }
			    request.setAttribute("tasks", filterTask);
			} else {
				request.setAttribute("tasks", tasks);
			}
			categories = categoryDao.getAll();
			statuses = statusDao.getAll();
			users = userDao.getAll();
	} catch (ClassNotFoundException | SQLException | IllegalArgumentException e) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
	}
		request.setAttribute("categories", categories);
		request.setAttribute("statuses", statuses);
		request.setAttribute("users", users);
		
		RequestDispatcher rd = request.getRequestDispatcher("task-list.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

}
