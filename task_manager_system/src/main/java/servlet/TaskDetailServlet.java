package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.CommentDao;
import model.dao.TaskDao;
import model.entity.CommentBean;
import model.entity.TaskBean;

/**
 * Servlet implementation class TaskDetailSurvlet
 */
@WebServlet("/task-detail-servlet")
public class TaskDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, IllegalArgumentException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
		int taskId = Integer.parseInt(request.getParameter("task-id"));
		
		request.setCharacterEncoding("UTF-8");
		
		TaskBean task = new TaskBean();
		TaskDao taskDao = new TaskDao();
		
		List<CommentBean> comments = new ArrayList<CommentBean>();
		CommentDao commentDao = new CommentDao();
		
		try {
			task = taskDao.get(taskId);
			comments = commentDao.search(taskId);
		} catch (ClassNotFoundException | SQLException | IllegalArgumentException e) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
		request.setAttribute("task", task);
		request.setAttribute("comments", comments);

		
		RequestDispatcher rd = request.getRequestDispatcher("task-detail.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, IllegalArgumentException {
		
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		

    }
	
}

