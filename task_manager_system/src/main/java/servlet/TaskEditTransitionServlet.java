package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.CategoryDao;
import model.dao.StatusDao;
import model.dao.TaskDao;
import model.dao.UserDao;
import model.entity.CategoryBean;
import model.entity.StatusBean;
import model.entity.TaskBean;
import model.entity.UserBean;

/**
 * Servlet implementation class TaskEditTransitionServlet
 */
@WebServlet("/task-edit-transition-servlet")
public class TaskEditTransitionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskEditTransitionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		// リクエストパラメーターからタスクIDを取得
		int taskId = Integer.parseInt(request.getParameter("taskId"));
		
		CategoryDao categoryDao = new CategoryDao();
		StatusDao statusDao = new StatusDao();
		UserDao userDao = new UserDao();
		TaskDao taskDao = new TaskDao();
		
		TaskBean task = new TaskBean();
		
		List<CategoryBean> categoryList = new ArrayList<CategoryBean>();
		List<StatusBean> statusList = new ArrayList<StatusBean>();
		List<UserBean> userList = new ArrayList<UserBean>();
		
		String memo = "";
		
		try {
			
			// 取得したタスクIDと一致するタスク名を格納
			task = taskDao.get(taskId);
			
			// 各DAOでDBから取得したカテゴリ、ステータス、ユーザー、メモ
			// 全てを各リストに格納
			categoryList = categoryDao.getAll();
			statusList = statusDao.getAll();
			userList = userDao.getAll();
			memo = task.getMemo();
			
			// リクエストスコープへリストとTaskBeanオブジェクトを設定
			request.setAttribute("categoryList", categoryList);
			request.setAttribute("statusList", statusList);
			request.setAttribute("userList", userList);
			request.setAttribute("memo", memo);
			request.setAttribute("task", task);
			RequestDispatcher rd = request.getRequestDispatcher
					("task-edit.jsp");
			rd.forward(request, response);
			
		} catch (IllegalArgumentException | ClassNotFoundException | SQLException e) {
			String errorMessage = (String) request.getAttribute("errorMessage");
			request.setAttribute("errorMessage", errorMessage);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-list.jsp");
			requestDispatcher.forward(request, response);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
