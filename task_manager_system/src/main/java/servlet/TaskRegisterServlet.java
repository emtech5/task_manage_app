package servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.RegisterNotification;
import model.dao.CategoryDao;
import model.dao.StatusDao;
import model.dao.TaskDao;
import model.dao.UserDao;
import model.entity.CategoryBean;
import model.entity.TaskBean;
import model.exception.DuplicationExpection;

/**
 * Servlet implementation class TaskRegister
 */
@WebServlet("/task-register-servlet")
public class TaskRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TaskRegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}

		request.setCharacterEncoding("UTF-8");

		try {
			CategoryDao categoryDao = new CategoryDao();

			String taskName	= request.getParameter("taskName");
			if ( "".equals(taskName) ) {
				String errorMessage = (String)request.getAttribute("errorMessage");
				if (errorMessage == null || errorMessage.isEmpty()) {
					request.setAttribute("errorMessage", "Please enter something in the Task Name.<br>");
				} else {
					request.setAttribute("errorMessage", (String)request.getAttribute("errorMessage") + "Please enter something in the Task Name.<br>");
				}
			}
			String newOrExist = request.getParameter("new_or_exist");
			int existCategoryId = Integer.parseInt(request.getParameter("existCategoryId"));
			String newCategoryName = request.getParameter("newCategoryName");
			String userId = request.getParameter("userId");
			String statusCode = request.getParameter("statusCode");
			String memo = request.getParameter("memo");		
			int CategoryId = 1;

			if ("new".equals(newOrExist)) { 
				try {
					categoryDao.check(newCategoryName);
				} catch (DuplicationExpection e) {
					String errorMessage = (String)request.getAttribute("errorMessage");
					if (errorMessage == null || errorMessage.isEmpty()) {
						request.setAttribute("errorMessage", "Category Name entered already exists.<br>");
					} else {
						request.setAttribute("errorMessage", (String)request.getAttribute("errorMessage") + "Category Name entered already exists.<br>");
					}	
					throw e;
				}
				if ("".equals(newCategoryName)) {
					String errorMessage = (String)request.getAttribute("errorMessage");
					if (errorMessage == null || errorMessage.isEmpty()) {
						request.setAttribute("errorMessage", "Please enter something in the Category Name.<br>");
					} else {
						request.setAttribute("errorMessage", (String)request.getAttribute("errorMessage") + "Please enter something in the Category Name.<br>");
					}	
					throw new IllegalArgumentException();
				}
				categoryDao.add(new CategoryBean(0,newCategoryName));
				int newCategoryId = categoryDao.getCategoryId(newCategoryName);				
				CategoryId = newCategoryId;
			}else {
				CategoryId = existCategoryId;
			}
			
			Date limitDate;				
			try {
				limitDate = Date.valueOf(request.getParameter("limitDate"));
			} catch (Exception e){
				String errorMessage = (String)request.getAttribute("errorMessage");
				if (errorMessage == null || errorMessage.isEmpty()) {
					request.setAttribute("errorMessage", "Please select Limit Date.<br>");
				} else {
					request.setAttribute("errorMessage", (String)request.getAttribute("errorMessage") + "Please select Limit Date.<br>");
				}	
				throw e;
			}
			
			TaskDao taskDao = new TaskDao();
			UserDao userDao = new UserDao();
			StatusDao statusDao = new StatusDao();


			if ( taskName.isEmpty() ) {
				request.setAttribute("errorChecker", "error");
				String errorMessage = (String)request.getAttribute("errorMessage");
				if (errorMessage == null || errorMessage.isEmpty()) {
					request.setAttribute("errorMessage", "Could not register.<br>");
				} else {
					request.setAttribute("errorMessage", "Could not register.<br>" + (String)request.getAttribute("errorMessage"));
				}
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-register-transition-servlet");
				requestDispatcher.forward(request, response);
			}
			taskDao.add(
					new TaskBean(1,
							taskName,
							categoryDao.get(CategoryId),
							limitDate,
							userDao.get(userId),
							statusDao.get(statusCode),
							memo,
							null,
							null)			
					);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-list-servlet");
			requestDispatcher.forward(request, response);
			
			RegisterNotification registerNotification = new RegisterNotification();
			try {
				registerNotification.sendMessage(Integer.parseInt(request.getParameter("taskId")), taskName);
			} catch (Exception e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}


		} catch (ClassNotFoundException | SQLException | IllegalArgumentException | DuplicationExpection e){
			e.printStackTrace();

			request.setAttribute("errorChecker", "error");
			String errorMessage = (String)request.getAttribute("errorMessage");
			if (errorMessage == null || errorMessage.isEmpty()) {
				request.setAttribute("errorMessage", "Could not register.<br>");
			} else {
				request.setAttribute("errorMessage", "Could not register.<br>" + (String)request.getAttribute("errorMessage"));
			}
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("task-register-transition-servlet");
			requestDispatcher.forward(request, response);
		}
	}

}
